<?php

return [
  'entities' => [
    'builder' => '\\Lara\\Common\\Models\\EntityCustomColumns',
    'page' => 'Lara\\Common\\Models\\Page',
    'team' => 'Lara\\Common\\Models\\Team',
    'tag' => 'Lara\\Common\\Models\\Tag',
    'setting' => 'Lara\\Common\\Models\\Setting',
    'menu' => 'Lara\\Common\\Models\\Menu',
    'portfolio' => '\\Lara\\Common\\Models\\Portfolio',
    'sponsor' => 'Lara\\Common\\Models\\Sponsor',
    'product' => 'Lara\\Common\\Models\\Product',
    'test' => 'Lara\\Common\\Models\\Test',
  ],
];