#!/bin/bash

if [[ $OSTYPE == 'linux-gnu' ]];
then
	PHP7PATH="/opt/plesk/php/8.1/bin/php"
else
	PHP7PATH="/Applications/MAMP/bin/php/php8.1.13/bin/php"
fi



"$PHP7PATH" artisan cache:clear
"$PHP7PATH" artisan config:cache
"$PHP7PATH" artisan view:clear
"$PHP7PATH" artisan httpcache:clear

rm -r ./storage/framework/cache/data/*
rm -r ./storage/framework/sessions/*
rm -r ./storage/framework/testing/*
rm -r ./storage/framework/views/*
rm -r ./storage/httpcache/*
rm -r ./bootstrap/cache/*
