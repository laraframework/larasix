<?php

return [
	'column' => [
		'name' => 'name',
		'title' => 'title',
	],
	'entity' => [
		'entity_plural' => 'reactions',
		'entity_single' => 'reaction',
		'entity_title' => 'reactions',
	],
];
