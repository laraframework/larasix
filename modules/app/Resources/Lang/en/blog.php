<?php

return [
	'column' => [
		'body' => 'text',
		'lead' => 'excerpt',
		'location' => 'location',
		'source' => 'source',
		'sticky' => 'sticky',
		'title' => 'title',
	],
	'entity' => [
		'entity_plural' => 'news items',
		'entity_single' => 'news item',
		'entity_title' => 'news',
	],
];
