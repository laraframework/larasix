<?php

return [
	'column' => [
		'lead' => 'quote',
		'role' => 'role',
		'title' => 'name',
	],
	'entity' => [
		'entity_plural' => 'testimonials',
		'entity_single' => 'testimonial',
		'entity_title' => 'testimonials',
	],
];
