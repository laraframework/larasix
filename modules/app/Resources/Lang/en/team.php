<?php

return [
	'column' => [
		'body' => 'text',
		'email' => 'email',
		'firstname' => 'first name',
		'lastname' => 'last name',
		'lead' => 'excerpt',
		'middlename' => 'middle name',
		'role' => 'role',
		'title' => 'last name',
	],
	'entity' => [
		'entity_plural' => 'teams',
		'entity_single' => 'team',
		'entity_title' => 'team',
	],
];
