<?php

return [
	'column' => [
		'body' => 'tekst',
		'lead' => 'inleiding',
		'location' => 'locatie',
		'source' => 'bron',
		'sticky' => 'sticky',
		'title' => 'titel',
	],
	'entity' => [
		'entity_plural' => 'nieuwsberichten',
		'entity_single' => 'nieuwsbericht',
		'entity_title' => 'nieuws',
	],
];
