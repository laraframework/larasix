<?php

return [
	'column' => [
		'lead' => 'quote',
		'role' => 'rol',
		'title' => 'naam',
	],
	'entity' => [
		'entity_plural' => 'testimonials',
		'entity_single' => 'testimonial',
		'entity_title' => 'testimonials',
	],
];
