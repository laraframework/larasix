<?php

return [
	'column' => [
		'body' => 'tekst',
		'email' => 'email',
		'firstname' => 'voornaam',
		'lastname' => 'achternaam',
		'lead' => 'inleiding',
		'middlename' => 'tussenvoegsel',
		'role' => 'rol',
		'title' => 'achternaam',
	],
	'entity' => [
		'entity_plural' => 'teams',
		'entity_single' => 'team',
		'entity_title' => 'team',
	],
];
