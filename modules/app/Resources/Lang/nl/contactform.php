<?php

return [
	'column' => [
		'name' => 'naam',
		'title' => 'titel',
	],
	'entity' => [
		'entity_plural' => 'reacties',
		'entity_single' => 'reactie',
		'entity_title' => 'reacties',
	],
];
