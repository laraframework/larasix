<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraSysUploadsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_sys_uploads')->delete();
        
        \DB::table('lara_sys_uploads')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'entity_type' => 'Eve\\Models\\Post',
                'object_id' => 0,
                'token' => 'QzmxamANPKfCXG13Zmnkvjd2IZBV57fYv7q2CTRx',
                'filename' => '20211007141927-haarlem-sky.jpg',
                'filetype' => 'image',
                'mimetype' => 'image/jpeg',
                'created_at' => '2021-10-07 14:19:27',
                'updated_at' => '2021-10-07 14:19:27',
                'dz_session_id' => '20211007141903',
            ),
            1 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'entity_type' => 'Eve\\Models\\Blog',
                'object_id' => 3,
                'token' => 'XUPjaehlE86eaW3lNbW5z5Mw25EcpFakKcO0T9pd',
                'filename' => '20230207125504-test.mp4',
                'filetype' => 'videofile',
                'mimetype' => 'video/mp4',
                'created_at' => '2023-02-07 12:55:04',
                'updated_at' => '2023-02-07 12:55:04',
                'dz_session_id' => '20230207125459',
            ),
        ));
        
        
    }
}