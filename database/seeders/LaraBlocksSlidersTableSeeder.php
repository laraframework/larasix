<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraBlocksSlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_blocks_sliders')->delete();
        
        \DB::table('lara_blocks_sliders')->insert(array (
            0 => 
            array (
                'id' => 7,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'The Best Solutions for Your Business',
                'subtitle' => NULL,
                'textposition' => 'center',
                'overlaysize' => 'full',
                'overlaytransp' => '40',
                'overlaycolor' => 'black',
                'urltext' => 'Start your project',
                'urltitle' => 'Start your project',
                'url' => '#',
                'payoff' => '<p>We provide the wide range of high quality services and best practices solutions<br />to our customers making their business better.</p>',
                'captiontype' => 'block',
                'caption' => '',
                'type' => 'payoff',
                'slug' => 'the-best-solutions-for-your-business',
                'slug_lock' => 0,
                'created_at' => '2017-10-09 22:35:03',
                'updated_at' => '2023-03-10 15:24:53',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_from' => '2017-10-09 22:35:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
                'position' => NULL,
            ),
            1 => 
            array (
                'id' => 204,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'Award-Winning Software Company',
                'subtitle' => NULL,
                'textposition' => 'center',
                'overlaysize' => 'full',
                'overlaytransp' => '40',
                'overlaycolor' => 'black',
                'urltext' => 'Get in touch',
                'urltitle' => 'Get in touch',
                'url' => '#',
                'payoff' => '<p>We are a dedicated team of passionate product managers, full stack developers, UX/UI designers</p>',
                'captiontype' => 'block',
                'caption' => '',
                'type' => 'payoff',
                'slug' => 'award-winning-software-company',
                'slug_lock' => 0,
                'created_at' => '2023-03-09 15:35:38',
                'updated_at' => '2023-03-10 15:25:19',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_from' => '2023-03-09 00:00:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
                'position' => 1,
            ),
            2 => 
            array (
                'id' => 205,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'This Technology May Change the World',
                'subtitle' => NULL,
                'textposition' => 'center',
                'overlaysize' => 'full',
                'overlaytransp' => '40',
                'overlaycolor' => 'black',
                'urltext' => 'Join our team',
                'urltitle' => 'Join our team',
                'url' => '#',
                'payoff' => '<p>This Content Management System is fast, stable, and has an incredible user experience</p>',
                'captiontype' => 'block',
                'caption' => 'We create complex, web and mobile solutions for any business need',
                'type' => 'payoff',
                'slug' => 'this-technology-may-change-the-world',
                'slug_lock' => 0,
                'created_at' => '2023-03-09 15:47:57',
                'updated_at' => '2023-03-10 18:07:18',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_from' => '2023-03-09 15:48:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
                'position' => 2,
            ),
            3 => 
            array (
                'id' => 206,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'Cashless payment case study',
                'subtitle' => 'Payment Service Provider Company',
                'textposition' => 'center',
                'overlaysize' => 'full',
                'overlaytransp' => '10',
                'overlaycolor' => 'black',
                'urltext' => 'View case study',
                'urltitle' => 'Cashless payment case study',
                'url' => '#',
                'payoff' => '<p>Aenean dolor elit tempus tellus imperdiet. Elementum, ac convallis morbi sit est feugiat ultrices. Cras tortor maecenas pulvinar nec ac justo. Massa sem eget semper...</p>',
                'captiontype' => '',
                'caption' => '',
                'type' => 'payoff',
                'slug' => 'cashless-payment-case-study',
                'slug_lock' => 0,
                'created_at' => '2023-03-11 14:38:31',
                'updated_at' => '2023-03-11 19:51:46',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_from' => '2023-03-11 14:43:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
                'position' => 3,
            ),
            4 => 
            array (
                'id' => 207,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'Smart tech case study',
                'subtitle' => 'Data Analytics Company',
                'textposition' => 'center',
                'overlaysize' => 'full',
                'overlaytransp' => '',
                'overlaycolor' => 'black',
                'urltext' => '',
                'urltitle' => '',
                'url' => '',
                'payoff' => '<p>Adipiscing quis a at ligula. Gravida gravida diam rhoncus cursus in. Turpis sagittis tempor gravida phasellus sapien. Faucibus donec consectetur sed id sit nisl.</p>',
                'captiontype' => '',
                'caption' => '',
                'type' => 'payoff',
                'slug' => 'smart-tech-case-study',
                'slug_lock' => 0,
                'created_at' => '2023-03-11 18:13:59',
                'updated_at' => '2023-03-11 19:52:00',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_from' => '2023-03-11 18:15:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
                'position' => 4,
            ),
        ));
        
        
    }
}