<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraAuthHasAbilitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_auth_has_abilities')->delete();
        
        \DB::table('lara_auth_has_abilities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'ability_id' => 2,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'ability_id' => 4,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'ability_id' => 3,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'ability_id' => 1,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'ability_id' => 38,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'ability_id' => 40,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'ability_id' => 39,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'ability_id' => 37,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            8 => 
            array (
                'id' => 13,
                'ability_id' => 192,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            9 => 
            array (
                'id' => 14,
                'ability_id' => 194,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            10 => 
            array (
                'id' => 15,
                'ability_id' => 193,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            11 => 
            array (
                'id' => 16,
                'ability_id' => 191,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            12 => 
            array (
                'id' => 17,
                'ability_id' => 289,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            13 => 
            array (
                'id' => 18,
                'ability_id' => 290,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            14 => 
            array (
                'id' => 19,
                'ability_id' => 291,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            15 => 
            array (
                'id' => 20,
                'ability_id' => 292,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            16 => 
            array (
                'id' => 21,
                'ability_id' => 293,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            17 => 
            array (
                'id' => 22,
                'ability_id' => 294,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            18 => 
            array (
                'id' => 23,
                'ability_id' => 295,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            19 => 
            array (
                'id' => 24,
                'ability_id' => 296,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            20 => 
            array (
                'id' => 25,
                'ability_id' => 297,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            21 => 
            array (
                'id' => 26,
                'ability_id' => 298,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            22 => 
            array (
                'id' => 27,
                'ability_id' => 299,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            23 => 
            array (
                'id' => 28,
                'ability_id' => 300,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            24 => 
            array (
                'id' => 29,
                'ability_id' => 309,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            25 => 
            array (
                'id' => 30,
                'ability_id' => 310,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            26 => 
            array (
                'id' => 31,
                'ability_id' => 311,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            27 => 
            array (
                'id' => 32,
                'ability_id' => 312,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            28 => 
            array (
                'id' => 33,
                'ability_id' => 313,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            29 => 
            array (
                'id' => 34,
                'ability_id' => 314,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            30 => 
            array (
                'id' => 35,
                'ability_id' => 315,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            31 => 
            array (
                'id' => 36,
                'ability_id' => 316,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            32 => 
            array (
                'id' => 41,
                'ability_id' => 321,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            33 => 
            array (
                'id' => 42,
                'ability_id' => 322,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            34 => 
            array (
                'id' => 43,
                'ability_id' => 323,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            35 => 
            array (
                'id' => 44,
                'ability_id' => 324,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            36 => 
            array (
                'id' => 45,
                'ability_id' => 325,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            37 => 
            array (
                'id' => 46,
                'ability_id' => 326,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            38 => 
            array (
                'id' => 47,
                'ability_id' => 327,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            39 => 
            array (
                'id' => 48,
                'ability_id' => 328,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            40 => 
            array (
                'id' => 53,
                'ability_id' => 333,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            41 => 
            array (
                'id' => 54,
                'ability_id' => 334,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            42 => 
            array (
                'id' => 55,
                'ability_id' => 335,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            43 => 
            array (
                'id' => 56,
                'ability_id' => 336,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            44 => 
            array (
                'id' => 57,
                'ability_id' => 337,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            45 => 
            array (
                'id' => 58,
                'ability_id' => 338,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            46 => 
            array (
                'id' => 59,
                'ability_id' => 339,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            47 => 
            array (
                'id' => 60,
                'ability_id' => 340,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            48 => 
            array (
                'id' => 69,
                'ability_id' => 385,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            49 => 
            array (
                'id' => 70,
                'ability_id' => 386,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            50 => 
            array (
                'id' => 71,
                'ability_id' => 387,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            51 => 
            array (
                'id' => 72,
                'ability_id' => 388,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            52 => 
            array (
                'id' => 93,
                'ability_id' => 417,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            53 => 
            array (
                'id' => 94,
                'ability_id' => 418,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            54 => 
            array (
                'id' => 95,
                'ability_id' => 419,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            55 => 
            array (
                'id' => 96,
                'ability_id' => 420,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            56 => 
            array (
                'id' => 97,
                'ability_id' => 421,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            57 => 
            array (
                'id' => 98,
                'ability_id' => 422,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            58 => 
            array (
                'id' => 99,
                'ability_id' => 423,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            59 => 
            array (
                'id' => 100,
                'ability_id' => 424,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            60 => 
            array (
                'id' => 101,
                'ability_id' => 425,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            61 => 
            array (
                'id' => 102,
                'ability_id' => 426,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            62 => 
            array (
                'id' => 103,
                'ability_id' => 427,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            63 => 
            array (
                'id' => 104,
                'ability_id' => 428,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            64 => 
            array (
                'id' => 105,
                'ability_id' => 429,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            65 => 
            array (
                'id' => 106,
                'ability_id' => 430,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            66 => 
            array (
                'id' => 107,
                'ability_id' => 431,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            67 => 
            array (
                'id' => 108,
                'ability_id' => 432,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            68 => 
            array (
                'id' => 109,
                'ability_id' => 433,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            69 => 
            array (
                'id' => 110,
                'ability_id' => 434,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            70 => 
            array (
                'id' => 111,
                'ability_id' => 435,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            71 => 
            array (
                'id' => 112,
                'ability_id' => 436,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            72 => 
            array (
                'id' => 113,
                'ability_id' => 437,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            73 => 
            array (
                'id' => 114,
                'ability_id' => 438,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            74 => 
            array (
                'id' => 115,
                'ability_id' => 439,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            75 => 
            array (
                'id' => 116,
                'ability_id' => 440,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            76 => 
            array (
                'id' => 117,
                'ability_id' => 441,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            77 => 
            array (
                'id' => 118,
                'ability_id' => 442,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            78 => 
            array (
                'id' => 119,
                'ability_id' => 443,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            79 => 
            array (
                'id' => 120,
                'ability_id' => 444,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            80 => 
            array (
                'id' => 121,
                'ability_id' => 445,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            81 => 
            array (
                'id' => 122,
                'ability_id' => 446,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            82 => 
            array (
                'id' => 123,
                'ability_id' => 447,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            83 => 
            array (
                'id' => 124,
                'ability_id' => 448,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            84 => 
            array (
                'id' => 125,
                'ability_id' => 449,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            85 => 
            array (
                'id' => 126,
                'ability_id' => 450,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            86 => 
            array (
                'id' => 127,
                'ability_id' => 451,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            87 => 
            array (
                'id' => 128,
                'ability_id' => 452,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            88 => 
            array (
                'id' => 129,
                'ability_id' => 453,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            89 => 
            array (
                'id' => 130,
                'ability_id' => 454,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            90 => 
            array (
                'id' => 131,
                'ability_id' => 455,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            91 => 
            array (
                'id' => 132,
                'ability_id' => 456,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            92 => 
            array (
                'id' => 133,
                'ability_id' => 457,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            93 => 
            array (
                'id' => 134,
                'ability_id' => 458,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            94 => 
            array (
                'id' => 135,
                'ability_id' => 459,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            95 => 
            array (
                'id' => 136,
                'ability_id' => 460,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            96 => 
            array (
                'id' => 137,
                'ability_id' => 461,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            97 => 
            array (
                'id' => 138,
                'ability_id' => 462,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            98 => 
            array (
                'id' => 139,
                'ability_id' => 463,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            99 => 
            array (
                'id' => 140,
                'ability_id' => 464,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            100 => 
            array (
                'id' => 141,
                'ability_id' => 465,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            101 => 
            array (
                'id' => 142,
                'ability_id' => 466,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            102 => 
            array (
                'id' => 143,
                'ability_id' => 467,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            103 => 
            array (
                'id' => 144,
                'ability_id' => 468,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            104 => 
            array (
                'id' => 145,
                'ability_id' => 469,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            105 => 
            array (
                'id' => 146,
                'ability_id' => 470,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            106 => 
            array (
                'id' => 147,
                'ability_id' => 471,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            107 => 
            array (
                'id' => 148,
                'ability_id' => 472,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            108 => 
            array (
                'id' => 589,
                'ability_id' => 473,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            109 => 
            array (
                'id' => 590,
                'ability_id' => 474,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            110 => 
            array (
                'id' => 591,
                'ability_id' => 475,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            111 => 
            array (
                'id' => 592,
                'ability_id' => 476,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            112 => 
            array (
                'id' => 593,
                'ability_id' => 477,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            113 => 
            array (
                'id' => 594,
                'ability_id' => 478,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            114 => 
            array (
                'id' => 595,
                'ability_id' => 479,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            115 => 
            array (
                'id' => 596,
                'ability_id' => 480,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            116 => 
            array (
                'id' => 685,
                'ability_id' => 481,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            117 => 
            array (
                'id' => 686,
                'ability_id' => 482,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            118 => 
            array (
                'id' => 687,
                'ability_id' => 483,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            119 => 
            array (
                'id' => 688,
                'ability_id' => 484,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            120 => 
            array (
                'id' => 1794,
                'ability_id' => 2,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            121 => 
            array (
                'id' => 1795,
                'ability_id' => 4,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            122 => 
            array (
                'id' => 1796,
                'ability_id' => 3,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            123 => 
            array (
                'id' => 1797,
                'ability_id' => 1,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            124 => 
            array (
                'id' => 1798,
                'ability_id' => 38,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            125 => 
            array (
                'id' => 1799,
                'ability_id' => 40,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            126 => 
            array (
                'id' => 1800,
                'ability_id' => 39,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            127 => 
            array (
                'id' => 1801,
                'ability_id' => 37,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            128 => 
            array (
                'id' => 1802,
                'ability_id' => 462,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            129 => 
            array (
                'id' => 1803,
                'ability_id' => 464,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            130 => 
            array (
                'id' => 1804,
                'ability_id' => 463,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            131 => 
            array (
                'id' => 1805,
                'ability_id' => 461,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            132 => 
            array (
                'id' => 1806,
                'ability_id' => 192,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            133 => 
            array (
                'id' => 1807,
                'ability_id' => 194,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            134 => 
            array (
                'id' => 1808,
                'ability_id' => 193,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            135 => 
            array (
                'id' => 1809,
                'ability_id' => 191,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            136 => 
            array (
                'id' => 1810,
                'ability_id' => 430,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            137 => 
            array (
                'id' => 1811,
                'ability_id' => 432,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            138 => 
            array (
                'id' => 1812,
                'ability_id' => 431,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            139 => 
            array (
                'id' => 1813,
                'ability_id' => 429,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            140 => 
            array (
                'id' => 1814,
                'ability_id' => 434,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            141 => 
            array (
                'id' => 1815,
                'ability_id' => 436,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            142 => 
            array (
                'id' => 1816,
                'ability_id' => 435,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            143 => 
            array (
                'id' => 1817,
                'ability_id' => 433,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            144 => 
            array (
                'id' => 1818,
                'ability_id' => 442,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            145 => 
            array (
                'id' => 1819,
                'ability_id' => 444,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            146 => 
            array (
                'id' => 1820,
                'ability_id' => 443,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            147 => 
            array (
                'id' => 1821,
                'ability_id' => 441,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            148 => 
            array (
                'id' => 1822,
                'ability_id' => 453,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            149 => 
            array (
                'id' => 1823,
                'ability_id' => 438,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            150 => 
            array (
                'id' => 1824,
                'ability_id' => 440,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            151 => 
            array (
                'id' => 1825,
                'ability_id' => 439,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            152 => 
            array (
                'id' => 1826,
                'ability_id' => 437,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            153 => 
            array (
                'id' => 1827,
                'ability_id' => 422,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            154 => 
            array (
                'id' => 1828,
                'ability_id' => 424,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            155 => 
            array (
                'id' => 1829,
                'ability_id' => 423,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            156 => 
            array (
                'id' => 1830,
                'ability_id' => 421,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            157 => 
            array (
                'id' => 1831,
                'ability_id' => 470,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            158 => 
            array (
                'id' => 1832,
                'ability_id' => 472,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            159 => 
            array (
                'id' => 1833,
                'ability_id' => 471,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            160 => 
            array (
                'id' => 1834,
                'ability_id' => 469,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            161 => 
            array (
                'id' => 1835,
                'ability_id' => 386,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            162 => 
            array (
                'id' => 1836,
                'ability_id' => 388,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            163 => 
            array (
                'id' => 1837,
                'ability_id' => 387,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            164 => 
            array (
                'id' => 1838,
                'ability_id' => 385,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            165 => 
            array (
                'id' => 1839,
                'ability_id' => 290,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            166 => 
            array (
                'id' => 1840,
                'ability_id' => 292,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            167 => 
            array (
                'id' => 1841,
                'ability_id' => 291,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            168 => 
            array (
                'id' => 1842,
                'ability_id' => 289,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            169 => 
            array (
                'id' => 1843,
                'ability_id' => 294,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            170 => 
            array (
                'id' => 1844,
                'ability_id' => 296,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            171 => 
            array (
                'id' => 1845,
                'ability_id' => 295,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            172 => 
            array (
                'id' => 1846,
                'ability_id' => 293,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            173 => 
            array (
                'id' => 1847,
                'ability_id' => 298,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            174 => 
            array (
                'id' => 1848,
                'ability_id' => 300,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            175 => 
            array (
                'id' => 1849,
                'ability_id' => 299,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            176 => 
            array (
                'id' => 1850,
                'ability_id' => 297,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            177 => 
            array (
                'id' => 1851,
                'ability_id' => 310,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            178 => 
            array (
                'id' => 1852,
                'ability_id' => 312,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            179 => 
            array (
                'id' => 1853,
                'ability_id' => 311,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            180 => 
            array (
                'id' => 1854,
                'ability_id' => 309,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            181 => 
            array (
                'id' => 1855,
                'ability_id' => 314,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            182 => 
            array (
                'id' => 1856,
                'ability_id' => 316,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            183 => 
            array (
                'id' => 1857,
                'ability_id' => 315,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            184 => 
            array (
                'id' => 1858,
                'ability_id' => 313,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            185 => 
            array (
                'id' => 1859,
                'ability_id' => 466,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            186 => 
            array (
                'id' => 1860,
                'ability_id' => 468,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            187 => 
            array (
                'id' => 1861,
                'ability_id' => 467,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            188 => 
            array (
                'id' => 1862,
                'ability_id' => 465,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            189 => 
            array (
                'id' => 1863,
                'ability_id' => 322,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            190 => 
            array (
                'id' => 1864,
                'ability_id' => 324,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            191 => 
            array (
                'id' => 1865,
                'ability_id' => 323,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            192 => 
            array (
                'id' => 1866,
                'ability_id' => 321,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            193 => 
            array (
                'id' => 1867,
                'ability_id' => 326,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            194 => 
            array (
                'id' => 1868,
                'ability_id' => 328,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            195 => 
            array (
                'id' => 1869,
                'ability_id' => 327,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            196 => 
            array (
                'id' => 1870,
                'ability_id' => 325,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            197 => 
            array (
                'id' => 1871,
                'ability_id' => 334,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            198 => 
            array (
                'id' => 1872,
                'ability_id' => 336,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            199 => 
            array (
                'id' => 1873,
                'ability_id' => 335,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            200 => 
            array (
                'id' => 1874,
                'ability_id' => 333,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            201 => 
            array (
                'id' => 1875,
                'ability_id' => 338,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            202 => 
            array (
                'id' => 1876,
                'ability_id' => 340,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            203 => 
            array (
                'id' => 1877,
                'ability_id' => 339,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            204 => 
            array (
                'id' => 1878,
                'ability_id' => 337,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 2,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            205 => 
            array (
                'id' => 1879,
                'ability_id' => 485,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            206 => 
            array (
                'id' => 1880,
                'ability_id' => 486,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            207 => 
            array (
                'id' => 1881,
                'ability_id' => 487,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            208 => 
            array (
                'id' => 1882,
                'ability_id' => 488,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            209 => 
            array (
                'id' => 1883,
                'ability_id' => 489,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            210 => 
            array (
                'id' => 1884,
                'ability_id' => 490,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            211 => 
            array (
                'id' => 1885,
                'ability_id' => 491,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
            212 => 
            array (
                'id' => 1886,
                'ability_id' => 492,
                'entity_type' => 'lara_auth_roles',
                'entity_id' => 1,
                'forbidden' => 0,
                'scope' => NULL,
            ),
        ));
        
        
    }
}