<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraObjectLayoutTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_object_layout')->delete();
        
        \DB::table('lara_object_layout')->insert(array (
            0 => 
            array (
                'id' => 81,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 32,
                'layout_key' => 'content',
                'layout_value' => 'boxed_default_col_10',
                'created_at' => '2023-03-13 15:45:18',
                'updated_at' => '2023-03-13 15:45:18',
            ),
            1 => 
            array (
                'id' => 86,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 19,
                'layout_key' => 'content',
                'layout_value' => 'boxed_default_col_10',
                'created_at' => '2023-03-16 14:02:42',
                'updated_at' => '2023-03-16 14:02:42',
            ),
            2 => 
            array (
                'id' => 90,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1063,
                'layout_key' => 'content',
                'layout_value' => 'boxed_default_col_10',
                'created_at' => '2023-03-17 13:35:44',
                'updated_at' => '2023-03-17 13:35:44',
            ),
            3 => 
            array (
                'id' => 102,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1,
                'layout_key' => 'hero',
                'layout_value' => 'hero_slider_100',
                'created_at' => '2023-03-23 12:44:56',
                'updated_at' => '2023-03-23 12:44:56',
            ),
            4 => 
            array (
                'id' => 103,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1,
                'layout_key' => 'content',
                'layout_value' => 'full_width',
                'created_at' => '2023-03-23 12:44:56',
                'updated_at' => '2023-03-23 12:44:56',
            ),
            5 => 
            array (
                'id' => 104,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1,
                'layout_key' => 'cta',
                'layout_value' => 'hidden',
                'created_at' => '2023-03-23 12:44:56',
                'updated_at' => '2023-03-23 12:44:56',
            ),
            6 => 
            array (
                'id' => 105,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 16,
                'layout_key' => 'content',
                'layout_value' => 'boxed_default_col_10',
                'created_at' => '2023-03-23 14:13:10',
                'updated_at' => '2023-03-23 14:13:10',
            ),
            7 => 
            array (
                'id' => 106,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 16,
                'layout_key' => 'cta',
                'layout_value' => 'contact',
                'created_at' => '2023-03-23 14:13:10',
                'updated_at' => '2023-03-23 14:13:10',
            ),
            8 => 
            array (
                'id' => 115,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1090,
                'layout_key' => 'header',
                'layout_value' => 'header_classic',
                'created_at' => '2023-03-23 14:20:00',
                'updated_at' => '2023-03-23 14:20:00',
            ),
            9 => 
            array (
                'id' => 116,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1090,
                'layout_key' => 'cta',
                'layout_value' => 'contact',
                'created_at' => '2023-03-23 14:20:00',
                'updated_at' => '2023-03-23 14:20:00',
            ),
        ));
        
        
    }
}