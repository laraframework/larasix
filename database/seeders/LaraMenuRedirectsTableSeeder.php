<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraMenuRedirectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_menu_redirects')->delete();
        
        \DB::table('lara_menu_redirects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'language' => 'nl',
                'title' => 'test1',
                'redirectfrom' => 'test1',
                'redirectto' => 'test2',
                'redirecttype' => '301',
                'auto_generated' => 0,
                'locked_by_admin' => 0,
                'has_error' => 0,
                'created_at' => '2021-06-21 13:24:40',
                'updated_at' => '2023-03-02 14:49:47',
                'publish' => 1,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
        ));
        
        
    }
}