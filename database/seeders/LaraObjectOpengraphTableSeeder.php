<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraObjectOpengraphTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_object_opengraph')->delete();
        
        \DB::table('lara_object_opengraph')->insert(array (
            0 => 
            array (
                'id' => 23,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 257,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-10 15:28:43',
                'updated_at' => '2023-03-10 15:28:43',
            ),
            1 => 
            array (
                'id' => 24,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 258,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-10 16:38:24',
                'updated_at' => '2023-03-10 16:38:24',
            ),
            2 => 
            array (
                'id' => 25,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 259,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-10 16:41:07',
                'updated_at' => '2023-03-10 16:41:07',
            ),
            3 => 
            array (
                'id' => 26,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 260,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-10 16:41:51',
                'updated_at' => '2023-03-10 16:41:51',
            ),
            4 => 
            array (
                'id' => 27,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-10 18:07:28',
                'updated_at' => '2023-03-10 18:11:36',
            ),
            5 => 
            array (
                'id' => 28,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 16,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 16:55:45',
                'updated_at' => '2023-03-12 16:55:45',
            ),
            6 => 
            array (
                'id' => 29,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 446,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:38:52',
                'updated_at' => '2023-03-12 17:38:52',
            ),
            7 => 
            array (
                'id' => 30,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 447,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:40:22',
                'updated_at' => '2023-03-12 17:40:22',
            ),
            8 => 
            array (
                'id' => 31,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 448,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:41:22',
                'updated_at' => '2023-03-12 17:41:22',
            ),
            9 => 
            array (
                'id' => 32,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 449,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:42:02',
                'updated_at' => '2023-03-12 17:42:02',
            ),
            10 => 
            array (
                'id' => 33,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 450,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:43:22',
                'updated_at' => '2023-03-12 17:43:22',
            ),
            11 => 
            array (
                'id' => 34,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 451,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:44:13',
                'updated_at' => '2023-03-12 17:44:13',
            ),
            12 => 
            array (
                'id' => 35,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 452,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:44:55',
                'updated_at' => '2023-03-12 17:44:55',
            ),
            13 => 
            array (
                'id' => 36,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 453,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-12 17:46:44',
                'updated_at' => '2023-03-12 17:46:44',
            ),
            14 => 
            array (
                'id' => 37,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 261,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 11:55:27',
                'updated_at' => '2023-03-13 11:55:27',
            ),
            15 => 
            array (
                'id' => 38,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 262,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 11:57:29',
                'updated_at' => '2023-03-13 11:57:29',
            ),
            16 => 
            array (
                'id' => 39,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 32,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 12:41:59',
                'updated_at' => '2023-03-13 12:41:59',
            ),
            17 => 
            array (
                'id' => 40,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 263,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 13:41:04',
                'updated_at' => '2023-03-13 13:41:04',
            ),
            18 => 
            array (
                'id' => 41,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 264,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 13:47:09',
                'updated_at' => '2023-03-13 13:47:09',
            ),
            19 => 
            array (
                'id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 265,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 13:49:57',
                'updated_at' => '2023-03-13 13:49:57',
            ),
            20 => 
            array (
                'id' => 43,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 266,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 14:08:06',
                'updated_at' => '2023-03-13 14:08:06',
            ),
            21 => 
            array (
                'id' => 44,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 267,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-13 14:10:06',
                'updated_at' => '2023-03-13 14:10:06',
            ),
            22 => 
            array (
                'id' => 45,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 53,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-16 12:08:45',
                'updated_at' => '2023-03-16 12:08:45',
            ),
            23 => 
            array (
                'id' => 46,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 19,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-16 14:00:27',
                'updated_at' => '2023-03-16 14:00:27',
            ),
            24 => 
            array (
                'id' => 47,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 59,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-16 15:07:34',
                'updated_at' => '2023-03-16 15:07:34',
            ),
            25 => 
            array (
                'id' => 48,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1063,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-16 16:10:36',
                'updated_at' => '2023-03-16 16:10:36',
            ),
            26 => 
            array (
                'id' => 49,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1065,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-17 13:36:01',
                'updated_at' => '2023-03-17 13:36:01',
            ),
            27 => 
            array (
                'id' => 50,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1061,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-17 15:06:57',
                'updated_at' => '2023-03-17 15:06:57',
            ),
            28 => 
            array (
                'id' => 51,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1059,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-17 15:56:46',
                'updated_at' => '2023-03-17 15:56:46',
            ),
            29 => 
            array (
                'id' => 52,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1086,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-19 20:42:50',
                'updated_at' => '2023-03-19 20:42:50',
            ),
            30 => 
            array (
                'id' => 53,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1085,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-20 10:15:33',
                'updated_at' => '2023-03-20 10:15:33',
            ),
            31 => 
            array (
                'id' => 54,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1090,
                'og_title' => '',
                'og_description' => '',
                'og_image' => '',
                'created_at' => '2023-03-23 14:13:20',
                'updated_at' => '2023-03-23 14:13:20',
            ),
        ));
        
        
    }
}