<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraObjectVideosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_object_videos')->delete();
        
        \DB::table('lara_object_videos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 16,
                'title' => 'About us',
                'youtubecode' => 'OQQ4a4Os6cE',
                'featured' => 1,
                'created_at' => '2023-03-12 17:06:35',
                'updated_at' => '2023-03-13 11:51:12',
            ),
            1 => 
            array (
                'id' => 2,
                'entity_type' => 'Lara\\Common\\Models\\Page',
                'entity_id' => 1090,
                'title' => 'About us',
                'youtubecode' => 'OQQ4a4Os6cE',
                'featured' => 1,
                'created_at' => '2023-03-23 14:14:49',
                'updated_at' => '2023-03-23 14:14:49',
            ),
        ));
        
        
    }
}