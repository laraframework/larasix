<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraAuthUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_auth_users')->delete();
        
        \DB::table('lara_auth_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => 'api',
                'is_admin' => 0,
                'name' => 'API',
                'firstname' => '',
                'middlename' => '',
                'lastname' => '',
                'username' => NULL,
                'email' => '',
                'password' => '',
                'user_language' => NULL,
                'is_loggedin' => 0,
                'last_login' => NULL,
                'api_token' => 'O1tWONE28rTNQfC6E2LnZZ4MgSooWhwZ',
                'remember_token' => NULL,
                'email_verified_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'type' => 'web',
                'is_admin' => 1,
                'name' => 'Sybrand Hoeksma',
                'firstname' => 'Sybrand',
                'middlename' => NULL,
                'lastname' => 'Hoeksma',
                'username' => 'admin',
                'email' => 's.hoeksma@firmaq.nl',
                'password' => '$2y$10$RkSDHUMfhuGRjU.wZOMFZuMEcFUxWnn4B5tKT2L8iPdN83w0SMVT.',
                'user_language' => 'nl',
                'is_loggedin' => 1,
                'last_login' => '2023-03-23 14:27:54',
                'api_token' => '',
                'remember_token' => NULL,
                'email_verified_at' => '2021-01-01 12:00:00',
                'created_at' => '2021-01-01 12:00:00',
                'updated_at' => '2023-03-23 14:27:54',
                'deleted_at' => NULL,
                'locked_at' => '2023-03-17 10:48:55',
                'locked_by' => 2,
            ),
            2 => 
            array (
                'id' => 3,
                'type' => 'web',
                'is_admin' => 0,
                'name' => 'Cora Hoeksma-Kist',
                'firstname' => 'Cora',
                'middlename' => NULL,
                'lastname' => 'Hoeksma-Kist',
                'username' => 'kistc',
                'email' => 'cora.kist@firmaq.nl',
                'password' => '$2y$10$R01tkmlAohypGEdn9h6nFuUrce3Q7YQzphuDKUrkdABcJ.91xl3GW',
                'user_language' => 'nl',
                'is_loggedin' => 0,
                'last_login' => '2022-05-10 16:28:40',
                'api_token' => '',
                'remember_token' => NULL,
                'email_verified_at' => '2021-01-01 12:00:00',
                'created_at' => NULL,
                'updated_at' => '2023-02-13 13:09:05',
                'deleted_at' => NULL,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'type' => 'web',
                'is_admin' => 0,
                'name' => 'Front User',
                'firstname' => 'Front',
                'middlename' => NULL,
                'lastname' => 'User',
                'username' => 'frontuser',
                'email' => 'sh@firmaq.nl',
                'password' => '$2y$10$7tZNPQ8x61AnbJrJS5BRJOSPIZ832GRpBatb45E/glO3Fw0SEdMgu',
                'user_language' => 'nl',
                'is_loggedin' => 1,
                'last_login' => '2018-05-31 12:24:58',
                'api_token' => NULL,
                'remember_token' => NULL,
                'email_verified_at' => '2021-01-01 12:00:00',
                'created_at' => '2018-05-31 12:24:29',
                'updated_at' => '2022-12-27 15:27:26',
                'deleted_at' => NULL,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
            4 => 
            array (
                'id' => 7,
                'type' => 'web',
                'is_admin' => 0,
                'name' => 'guest',
                'firstname' => 'guest',
                'middlename' => NULL,
                'lastname' => 'guest',
                'username' => 'guest',
                'email' => 'guest@laracms.nl',
                'password' => '$2y$10$2iZY48rqOZ9kTG3E2ogBR.n4zyKExn0E9Zf5MqCbJxAvCIoEpTxh.',
                'user_language' => 'nl',
                'is_loggedin' => 0,
                'last_login' => NULL,
                'api_token' => NULL,
                'remember_token' => NULL,
                'email_verified_at' => '2021-01-01 12:00:00',
                'created_at' => '2021-04-26 14:28:33',
                'updated_at' => '2021-04-26 14:28:33',
                'deleted_at' => NULL,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
            5 => 
            array (
                'id' => 16,
                'type' => 'web',
                'is_admin' => 0,
                'name' => 'Jack Daniels',
                'firstname' => 'Jack',
                'middlename' => NULL,
                'lastname' => 'Daniels',
                'username' => 'jack',
                'email' => 's.hoeksma@outlook.com',
                'password' => '$2y$10$Mjw4tmIbQOv8gfxEAqUNWuoNfsVKr4AvDnw/mBegJ6Ngn3eLA8OGa',
                'user_language' => 'nl',
                'is_loggedin' => 0,
                'last_login' => '2021-06-28 16:11:07',
                'api_token' => NULL,
                'remember_token' => '589FGDHwgrd9dQNX8ETyKe1eYOPBW0HnGTeDCA4V7nqZjGC8B2lSueysVsDs',
                'email_verified_at' => '2021-01-01 12:00:00',
                'created_at' => '2021-06-25 13:10:09',
                'updated_at' => '2021-06-28 16:14:00',
                'deleted_at' => NULL,
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
        ));
        
        
    }
}