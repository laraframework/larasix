<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraAuthUsersProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_auth_users_profiles')->delete();
        
        \DB::table('lara_auth_users_profiles')->insert(array (
            0 => 
            array (
                'id' => 7,
                'user_id' => 2,
                'dark_mode' => 1,
                'created_at' => '2021-07-08 11:33:59',
                'updated_at' => '2021-10-18 13:44:55',
            ),
            1 => 
            array (
                'id' => 8,
                'user_id' => 16,
                'dark_mode' => 0,
                'created_at' => '2021-10-07 15:38:11',
                'updated_at' => '2021-10-07 15:38:11',
            ),
            2 => 
            array (
                'id' => 9,
                'user_id' => 7,
                'dark_mode' => 0,
                'created_at' => '2021-10-07 16:33:55',
                'updated_at' => '2021-10-07 16:33:55',
            ),
            3 => 
            array (
                'id' => 10,
                'user_id' => 4,
                'dark_mode' => 0,
                'created_at' => '2021-10-07 17:36:37',
                'updated_at' => '2021-10-07 17:36:37',
            ),
            4 => 
            array (
                'id' => 11,
                'user_id' => 3,
                'dark_mode' => 0,
                'created_at' => '2022-05-06 15:31:04',
                'updated_at' => '2022-05-06 15:31:13',
            ),
        ));
        
        
    }
}