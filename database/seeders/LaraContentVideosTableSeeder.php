<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraContentVideosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_content_videos')->delete();
        
        \DB::table('lara_content_videos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'language_parent' => NULL,
                'language' => 'nl',
                'title' => 'Lorem ipsum dolor sit amet 1',
                'youtubecode' => 'hEO09YiGF6k',
                'slug' => 'lorem-ipsum-dolor-sit-amet-1',
                'slug_lock' => 0,
                'created_at' => '2019-08-11 20:32:49',
                'updated_at' => '2021-10-18 15:50:50',
                'deleted_at' => NULL,
                'publish' => 1,
                'publish_hide' => 0,
                'publish_from' => '2019-08-11 00:00:00',
                'locked_at' => NULL,
                'locked_by' => NULL,
            ),
        ));
        
        
    }
}