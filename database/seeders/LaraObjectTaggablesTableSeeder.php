<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LaraObjectTaggablesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('lara_object_taggables')->delete();
        
        \DB::table('lara_object_taggables')->insert(array (
            0 => 
            array (
                'id' => 2,
                'tag_id' => 20,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 1,
            ),
            1 => 
            array (
                'id' => 3,
                'tag_id' => 22,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 2,
            ),
            2 => 
            array (
                'id' => 4,
                'tag_id' => 23,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 3,
            ),
            3 => 
            array (
                'id' => 5,
                'tag_id' => 20,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 5,
            ),
            4 => 
            array (
                'id' => 6,
                'tag_id' => 22,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 6,
            ),
            5 => 
            array (
                'id' => 7,
                'tag_id' => 23,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 7,
            ),
            6 => 
            array (
                'id' => 8,
                'tag_id' => 21,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 4,
            ),
            7 => 
            array (
                'id' => 9,
                'tag_id' => 21,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 8,
            ),
            8 => 
            array (
                'id' => 17,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 2,
            ),
            9 => 
            array (
                'id' => 18,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 1,
            ),
            10 => 
            array (
                'id' => 25,
                'tag_id' => 11,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 7,
            ),
            11 => 
            array (
                'id' => 26,
                'tag_id' => 1899,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 244,
            ),
            12 => 
            array (
                'id' => 27,
                'tag_id' => 1899,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 245,
            ),
            13 => 
            array (
                'id' => 28,
                'tag_id' => 1899,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 246,
            ),
            14 => 
            array (
                'id' => 29,
                'tag_id' => 1897,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 247,
            ),
            15 => 
            array (
                'id' => 30,
                'tag_id' => 1902,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 436,
            ),
            16 => 
            array (
                'id' => 31,
                'tag_id' => 1902,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 437,
            ),
            17 => 
            array (
                'id' => 32,
                'tag_id' => 1906,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 438,
            ),
            18 => 
            array (
                'id' => 33,
                'tag_id' => 1906,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 439,
            ),
            19 => 
            array (
                'id' => 34,
                'tag_id' => 1903,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 440,
            ),
            20 => 
            array (
                'id' => 35,
                'tag_id' => 1903,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 441,
            ),
            21 => 
            array (
                'id' => 36,
                'tag_id' => 1903,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 442,
            ),
            22 => 
            array (
                'id' => 37,
                'tag_id' => 1903,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 443,
            ),
            23 => 
            array (
                'id' => 38,
                'tag_id' => 1902,
                'entity_type' => 'Eve\\Models\\Team',
                'entity_id' => 444,
            ),
            24 => 
            array (
                'id' => 39,
                'tag_id' => 1914,
                'entity_type' => 'Eve\\Models\\Event',
                'entity_id' => 98,
            ),
            25 => 
            array (
                'id' => 40,
                'tag_id' => 1916,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 200,
            ),
            26 => 
            array (
                'id' => 41,
                'tag_id' => 1916,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 201,
            ),
            27 => 
            array (
                'id' => 42,
                'tag_id' => 1916,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 202,
            ),
            28 => 
            array (
                'id' => 43,
                'tag_id' => 1916,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 203,
            ),
            29 => 
            array (
                'id' => 45,
                'tag_id' => 1921,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 386,
            ),
            30 => 
            array (
                'id' => 46,
                'tag_id' => 1922,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 387,
            ),
            31 => 
            array (
                'id' => 47,
                'tag_id' => 1924,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 388,
            ),
            32 => 
            array (
                'id' => 48,
                'tag_id' => 1920,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 389,
            ),
            33 => 
            array (
                'id' => 49,
                'tag_id' => 1921,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 390,
            ),
            34 => 
            array (
                'id' => 50,
                'tag_id' => 1922,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 391,
            ),
            35 => 
            array (
                'id' => 51,
                'tag_id' => 1924,
                'entity_type' => 'Eve\\Models\\Portfolio',
                'entity_id' => 392,
            ),
            36 => 
            array (
                'id' => 53,
                'tag_id' => 1936,
                'entity_type' => 'Eve\\Models\\Story',
                'entity_id' => 1,
            ),
            37 => 
            array (
                'id' => 56,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 3,
            ),
            38 => 
            array (
                'id' => 65,
                'tag_id' => 1966,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 254,
            ),
            39 => 
            array (
                'id' => 66,
                'tag_id' => 1939,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 254,
            ),
            40 => 
            array (
                'id' => 67,
                'tag_id' => 11,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 204,
            ),
            41 => 
            array (
                'id' => 68,
                'tag_id' => 11,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 205,
            ),
            42 => 
            array (
                'id' => 69,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 257,
            ),
            43 => 
            array (
                'id' => 70,
                'tag_id' => 1938,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 257,
            ),
            44 => 
            array (
                'id' => 71,
                'tag_id' => 1939,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 258,
            ),
            45 => 
            array (
                'id' => 72,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 258,
            ),
            46 => 
            array (
                'id' => 73,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 259,
            ),
            47 => 
            array (
                'id' => 74,
                'tag_id' => 1969,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 259,
            ),
            48 => 
            array (
                'id' => 75,
                'tag_id' => 42,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 260,
            ),
            49 => 
            array (
                'id' => 76,
                'tag_id' => 1938,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 260,
            ),
            50 => 
            array (
                'id' => 77,
                'tag_id' => 1970,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 206,
            ),
            51 => 
            array (
                'id' => 78,
                'tag_id' => 1970,
                'entity_type' => 'Lara\\Common\\Models\\Slider',
                'entity_id' => 207,
            ),
            52 => 
            array (
                'id' => 79,
                'tag_id' => 1973,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 1,
            ),
            53 => 
            array (
                'id' => 80,
                'tag_id' => 1973,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 2,
            ),
            54 => 
            array (
                'id' => 81,
                'tag_id' => 1973,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 3,
            ),
            55 => 
            array (
                'id' => 82,
                'tag_id' => 1974,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 4,
            ),
            56 => 
            array (
                'id' => 83,
                'tag_id' => 1974,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 5,
            ),
            57 => 
            array (
                'id' => 84,
                'tag_id' => 1974,
                'entity_type' => 'Eve\\Models\\Testimonial',
                'entity_id' => 6,
            ),
            58 => 
            array (
                'id' => 85,
                'tag_id' => 1975,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 261,
            ),
            59 => 
            array (
                'id' => 86,
                'tag_id' => 1976,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 262,
            ),
            60 => 
            array (
                'id' => 87,
                'tag_id' => 1977,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 263,
            ),
            61 => 
            array (
                'id' => 88,
                'tag_id' => 1976,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 264,
            ),
            62 => 
            array (
                'id' => 89,
                'tag_id' => 1939,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 265,
            ),
            63 => 
            array (
                'id' => 90,
                'tag_id' => 1979,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 266,
            ),
            64 => 
            array (
                'id' => 91,
                'tag_id' => 1977,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 267,
            ),
            65 => 
            array (
                'id' => 93,
                'tag_id' => 1980,
                'entity_type' => 'Eve\\Models\\Event',
                'entity_id' => 3,
            ),
            66 => 
            array (
                'id' => 94,
                'tag_id' => 1980,
                'entity_type' => 'Eve\\Models\\Event',
                'entity_id' => 2,
            ),
            67 => 
            array (
                'id' => 95,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 261,
            ),
            68 => 
            array (
                'id' => 96,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 264,
            ),
            69 => 
            array (
                'id' => 97,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 263,
            ),
            70 => 
            array (
                'id' => 98,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 265,
            ),
            71 => 
            array (
                'id' => 99,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 266,
            ),
            72 => 
            array (
                'id' => 100,
                'tag_id' => 1981,
                'entity_type' => 'Eve\\Models\\Blog',
                'entity_id' => 267,
            ),
            73 => 
            array (
                'id' => 101,
                'tag_id' => 1955,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 2,
            ),
            74 => 
            array (
                'id' => 102,
                'tag_id' => 1955,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 1,
            ),
            75 => 
            array (
                'id' => 103,
                'tag_id' => 1956,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 3,
            ),
            76 => 
            array (
                'id' => 104,
                'tag_id' => 1982,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 2,
            ),
            77 => 
            array (
                'id' => 105,
                'tag_id' => 1955,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 6,
            ),
            78 => 
            array (
                'id' => 106,
                'tag_id' => 1955,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 7,
            ),
            79 => 
            array (
                'id' => 107,
                'tag_id' => 1982,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 7,
            ),
            80 => 
            array (
                'id' => 108,
                'tag_id' => 1955,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 8,
            ),
            81 => 
            array (
                'id' => 109,
                'tag_id' => 1983,
                'entity_type' => 'Eve\\Models\\Doc',
                'entity_id' => 8,
            ),
            82 => 
            array (
                'id' => 110,
                'tag_id' => 1986,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 1,
            ),
            83 => 
            array (
                'id' => 111,
                'tag_id' => 1987,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 2,
            ),
            84 => 
            array (
                'id' => 112,
                'tag_id' => 1988,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 3,
            ),
            85 => 
            array (
                'id' => 113,
                'tag_id' => 1989,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 1,
            ),
            86 => 
            array (
                'id' => 114,
                'tag_id' => 1991,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 2,
            ),
            87 => 
            array (
                'id' => 115,
                'tag_id' => 1992,
                'entity_type' => 'Eve\\Models\\Service',
                'entity_id' => 3,
            ),
        ));
        
        
    }
}